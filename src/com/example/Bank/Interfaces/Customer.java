package com.example.Bank.Interfaces;

public interface Customer {
    public void retrait(double montant);
    public void depot(double montant);
}
