package com.example.Bank.Comptes;

import com.example.Bank.BankClient;

public class Courant extends Account {
    public Courant(String number, double solde, BankClient owner)
    {
        super(number, solde, owner);
    }

    @Override
    protected double calculInteret() {
        return solde * 0.03;
    }
}
