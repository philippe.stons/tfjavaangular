package com.example.Tests;

import com.example.Bank.BankClient;
import com.example.Bank.Employee;
import com.example.Bank.User;

public abstract class AbstractTest {
    private String membre1;

    public AbstractTest()
    {
        System.out.println("ABSTRACT TEST CLASS");
    }

    public void maMethode(User u)
    {
        if(u instanceof Employee)
        {
            System.out.println("EMPLOYEE");
        } else if(u instanceof BankClient)
        {
            System.out.println("BANK CLIENT");
        }
    }

    public abstract void maMethodeAbstraite();
}
