package com.example.Bank;

import com.example.Bank.Comptes.Account;

import java.util.ArrayList;

public class BankClient extends User {
    private ArrayList<Account> comptes;

    public ArrayList<Account> getComptes() {
        return comptes;
    }

    public void addAccount(Account compte)
    {
        this.comptes.add(compte);
    }

    public BankClient(String nom, String prenom, String username, String password)
    {
        super(nom, prenom, username, password);
        comptes = new ArrayList<>();
    }

    @Override
    public void testPoly() {
        System.out.println("Extended class");
    }
}
