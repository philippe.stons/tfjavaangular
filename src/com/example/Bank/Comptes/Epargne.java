package com.example.Bank.Comptes;

import com.example.Bank.BankClient;

import java.time.LocalDateTime;

public class Epargne extends Account {
    private LocalDateTime dernierRetrait;
    public Epargne(String number, double solde, BankClient owner) {
        super(number, solde, owner);
        this.dernierRetrait = LocalDateTime.now();
    }

    @Override
    public void retrait(double montant) {
        double ancienMontant = this.solde;
        super.retrait(montant);

        if(ancienMontant == this.solde)
        {
            this.dernierRetrait = LocalDateTime.now();
        }
    }

    @Override
    protected double calculInteret() {
        return solde * 0.0975;
    }
}
