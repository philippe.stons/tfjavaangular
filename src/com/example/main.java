package com.example;

import com.example.Bank.BankClient;
import com.example.Bank.Comptes.Courant;
import com.example.Bank.User;

public class main {
    public static void main(String[] args) {
        BankClient u = new BankClient("test", "test", "test", "test");
        Courant c1 = new Courant("000000", 0, u);

        c1.depot(500);
        c1.appliquerInteret();
        System.out.println(c1.getSolde());
    }

    public boolean checkClientRole(User u)
    {
        if(u instanceof BankClient)
        {
            return true;
        }
        return false;
    }
}
