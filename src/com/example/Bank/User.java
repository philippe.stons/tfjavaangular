package com.example.Bank;

import java.util.ArrayList;

public class User {
    protected String nom;
    protected String prenom;
    protected String username;
    protected String password;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPassword(String password) {
        this.password = hashPassword(password);
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User() {
        this("test", "test", "test", "test");
    }

    public User(String nom, String prenom) {
        this(nom, prenom, "test", "password");
    }

    public User(String nom, String prenom, String username, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = hashPassword(password);
    }

    public final boolean checkPassword(String password)
    {
        return this.password.equals(password);
    }

    protected String hashPassword(String password)
    {
        return password;
    }

    public void testPoly()
    {
        System.out.println("Base Class!");
    }

    @Override
    public String toString() {
        return "User{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
