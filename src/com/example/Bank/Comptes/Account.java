package com.example.Bank.Comptes;

import com.example.Bank.BankClient;
import com.example.Bank.Interfaces.Customer;
import com.example.Bank.Interfaces.Banker;
import com.example.Bank.User;

public abstract class Account implements Customer, Banker {
    String number;
    double solde;
    User owner;

    public Account(String number, double solde, BankClient owner) {
        this.number = number;
        this.solde = solde;
        this.owner = owner;
        owner.addAccount(this);
    }

    public String getNumber() {
        return number;
    }

    public double getSolde() {
        return solde;
    }

    public User getOwner() {
        return owner;
    }

    public void retrait(double montant)
    {
        if(montant >= 0 && montant <= this.solde)
        {
            this.solde -= montant;
        }
    }

    public void depot(double montant)
    {
        if(montant >= 0)
        {
            this.solde += montant;
        }
    }

    protected abstract double calculInteret();

    public void appliquerInteret()
    {
        this.solde += calculInteret();
    }
}
